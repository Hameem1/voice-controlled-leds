"""Controls an LED matrix using speech commands.


Usage:
Requires a 3x2 LED matrix connected to the Coral Dev Board according to a certain specification.

sudo python3 led_control.py     #sudo is needed because this code uses the GPIO pins
"""

import model
import argparse
import numpy as np
from periphery import GPIO
from edgetpu.basic.basic_engine import BasicEngine

# Setting the blink frequency in Hertz
blink_frequency = 10
_delay = round((1 / blink_frequency) / 2, 2)
default_state = False
# Setting default Lit up LED (cursor)
cursor_led = 'led_31'
# Initializing LEDs
led_11 = GPIO(6, 'out')
led_12 = GPIO(73, 'out')
led_21 = GPIO(7, 'out')
led_22 = GPIO(138, 'out')
led_31 = GPIO(8, 'out')
led_32 = GPIO(140, 'out')
# A mapping of LED names to their GPIO interfaces
leds = dict(led_11=led_11,
            led_12=led_12,
            led_21=led_21,
            led_22=led_22,
            led_31=led_31,
            led_32=led_32)
# Arranging the LEDs in a matrix representative of their placement
led_matrix = np.array([['led_11', 'led_12'],
                       ['led_21', 'led_22'],
                       ['led_31', 'led_32']])


def clear_leds():
    for led in np.nditer(led_matrix):
        leds[str(led)].write(False)
    print('\nLEDs cleared!\n')


def update_cursor(direction):
    print('Updating Cursor...\n')
    axis = 0
    move = 0
    if direction == 'up':
        axis = 0
        move = 1
    if direction == 'down':
        axis = 0
        move = -1
    if direction == 'right':
        axis = 1
        move = 1
    if direction == 'left':
        axis = 1
        move = -1

    r, c = np.where(led_matrix == cursor_led)
    r = int(r)
    c = int(c)

    # up and right use move=1, down and right use move=-1
    # up and down use axis=1, right and left use axis=0
    print('\nCurrent position = ', (r, c), '\n')
    target = np.roll(led_matrix, move, axis=axis)
    target = target[r][c]
    print('\nUpdated position = ', target, '\n')
    print('move = ', move, '\n')
    print('axis = ', axis, '\n')
    return target


def speech_control(command):
    global cursor_led
    # print('command = ', command)
    if command == 'start_program':
        leds[cursor_led].write(True)
        # print('Program started!\n')

    elif command == 'move_up':
        leds[cursor_led].write(False)
        cursor_led = update_cursor('up')
        leds[cursor_led].write(True)
        print("Cursor changed to :", cursor_led, '\n')

    elif command == 'move_down':
        leds[cursor_led].write(False)
        cursor_led = update_cursor('down')
        leds[cursor_led].write(True)
        print("Cursor changed to :", cursor_led, '\n')

    elif command == 'move_right':
        leds[cursor_led].write(False)
        cursor_led = update_cursor('right')
        leds[cursor_led].write(True)
        print("Cursor changed to :", cursor_led, '\n')

    elif command == 'move_left':
        leds[cursor_led].write(False)
        cursor_led = update_cursor('left')
        leds[cursor_led].write(True)
        print("Cursor changed to :", cursor_led, '\n')

    elif command == 'exit_program':
        clear_leds()
        raise KeyboardInterrupt

    else:
        print('Functionality not implemented yet for :', command, '\n')


def main():
    # Initializing an argument parser
    parser = argparse.ArgumentParser()
    # Adding arguments to the parser
    model.add_model_flags(parser)
    # Parsing the command-line arguments
    args = parser.parse_args()
    # Setting up the model parameters for classification
    mic = args.mic if args.mic is None else int(args.mic)
    engine = BasicEngine(args.model_file)
    sample_rate = int(args.sample_rate_hz)
    num_frames_hop = int(args.num_frames_hop)
    callback_func = speech_control
    labels_file = 'config/labels_gc2.raw.txt'
    commands_file = "config/led_commands.txt"

    # Running the speech detection model
    model.classify_audio(mic, engine, labels_file, commands_file,
                         dectection_callback=callback_func,
                         sample_rate_hz=sample_rate,
                         num_frames_hop=num_frames_hop)


if __name__ == '__main__':
    # Waiting a bit before starting the program
    # sleep(2)
    # print('\nSay "What can I say" at any point to get instructions regarding program operation.\n')
    try:
        main()
    except KeyboardInterrupt as k:
        print('Exiting program...\n')
    except Exception as e:
        print('Unknown error occurred:\n', e, '\n')
    finally:
        print('Program exited!\n')
